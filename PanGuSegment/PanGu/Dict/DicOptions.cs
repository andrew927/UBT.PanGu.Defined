﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PanGu.Dict
{
    public class DicOptions
    {
        /// <summary>
        /// 字典词库名称列表
        /// </summary>
        public List<string> Dic { get; set; }
    }
}
