﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PanGu.Dict
{
    /// <summary>
    /// 字典创建类
    /// </summary>
    public class DictionaryCreate
    {
        /// <summary>
        /// 创建字典
        /// </summary>
        /// <param name="dictDir">字典路径</param>
        /// <param name="dictName">字典名称，包括扩展名 eg:Dict.dct</param>
        /// <returns></returns>
        public static bool CreateDictionary(string dictDir, string dictName)
        {
            try
            {
                var dictionaryDir = Framework.Path.AppendDivision(dictDir, '\\');
                var filePath = dictionaryDir + dictName;
                if (!File.Exists(filePath))
                {
                    File.Create(filePath);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }

        }

    }
}
