﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace PanGu.Dict
{
    class DictionaryLoader
    {
        public static Framework.Lock Lock = new PanGu.Framework.Lock();

        private string _DictionaryDir;

        public string DictionaryDir
        {
            get
            {
                return _DictionaryDir;
            }
        }

        private DateTime _MainDictLastTime;
        private DateTime _ChsSingleLastTime;
        private DateTime _ChsName1LastTime;
        private DateTime _ChsName2LastTime;
        private DateTime _StopWordLastTime;
        private DateTime _SynonymLastTime;
        private DateTime _WildcardLastTime;

        private Thread _Thread;

        private DateTime GetLastTime(string fileName)
        {
            return System.IO.File.GetLastWriteTime(DictionaryDir + fileName);
        }

        public DictionaryLoader(string dictDir)
        {
            _DictionaryDir = Framework.Path.AppendDivision(dictDir, '\\');
            _MainDictLastTime = GetLastTime("Dict.dct");
            _ChsSingleLastTime = GetLastTime(Dict.ChsName.ChsSingleNameFileName);
            _ChsName1LastTime = GetLastTime(Dict.ChsName.ChsDoubleName1FileName);
            _ChsName2LastTime = GetLastTime(Dict.ChsName.ChsDoubleName2FileName);
            _StopWordLastTime = GetLastTime("Stopword.txt");
            _SynonymLastTime = GetLastTime(Dict.Synonym.SynonymFileName);
            _WildcardLastTime = GetLastTime(Dict.Wildcard.WildcardFileName);

            _Thread = new Thread(() =>
            {
                MonitorDictionary(null);

            });
            _Thread.IsBackground = true;
            _Thread.Start();
        }

        /// <summary>
        /// 字典加载 add by zhangpeng 2016-10-27
        /// </summary>
        /// <param name="dictDir">字典路径</param>
        /// <param name="dictName">字典名称，包括扩展名 eg:Dict.dct</param>
        public DictionaryLoader(string dictDir, string dictName)
        {
            _DictionaryDir = Framework.Path.AppendDivision(dictDir, '\\');
            _MainDictLastTime = GetLastTime((string.IsNullOrWhiteSpace(dictName) ? "Dict.dct" : dictName));
            _ChsSingleLastTime = GetLastTime(Dict.ChsName.ChsSingleNameFileName);
            _ChsName1LastTime = GetLastTime(Dict.ChsName.ChsDoubleName1FileName);
            _ChsName2LastTime = GetLastTime(Dict.ChsName.ChsDoubleName2FileName);
            _StopWordLastTime = GetLastTime("Stopword.txt");
            _SynonymLastTime = GetLastTime(Dict.Synonym.SynonymFileName);
            _WildcardLastTime = GetLastTime(Dict.Wildcard.WildcardFileName);

            _Thread = new Thread(() => MonitorDictionary((string.IsNullOrWhiteSpace(dictName) ? "Dict.dct" : dictName)));
            _Thread.IsBackground = true;
            _Thread.Start();
        }

        


        private bool MainDictChanged(string dictName)
        {
            try
            {
                var lastTime = GetLastTime((string.IsNullOrWhiteSpace(dictName) ? "Dict.dct" : dictName));
                return _MainDictLastTime != lastTime;
            }
            catch
            {
                return false;
            }
        }

        private bool ChsNameChanged()
        {
            try
            {
                return (_ChsSingleLastTime != GetLastTime(Dict.ChsName.ChsSingleNameFileName) ||
                    _ChsName1LastTime != GetLastTime(Dict.ChsName.ChsDoubleName1FileName) ||
                    _ChsName2LastTime != GetLastTime(Dict.ChsName.ChsDoubleName2FileName));
            }
            catch
            {
                return false;
            }
        }

        private bool StopWordChanged()
        {
            try
            {
                return _StopWordLastTime != GetLastTime("Stopword.txt");
            }
            catch
            {
                return false;
            }
        }

        private bool SynonymChanged()
        {
            try
            {
                return _SynonymLastTime != GetLastTime(Dict.Synonym.SynonymFileName);
            }
            catch
            {
                return false;
            }
        }

        private bool WildcardChanged()
        {
            try
            {
                return _WildcardLastTime != GetLastTime(Dict.Wildcard.WildcardFileName);
            }
            catch
            {
                return false;
            }

        }


        private void MonitorDictionary(string dictName)
        {
            while (true)
            {
                Thread.Sleep(30000);

                try
                {
                    if (MainDictChanged(dictName))
                    {
                        try
                        {
                            DictionaryLoader.Lock.Enter(PanGu.Framework.Lock.Mode.Mutex);
                            Segment._WordDictionary.Load(_DictionaryDir + (string.IsNullOrWhiteSpace(dictName) ? "Dict.dct" : dictName));
                            _MainDictLastTime = GetLastTime((string.IsNullOrWhiteSpace(dictName) ? "Dict.dct" : dictName));
                        }
                        finally
                        {
                            DictionaryLoader.Lock.Leave();
                        }
                    }

                    if (ChsNameChanged())
                    {
                        try
                        {
                            DictionaryLoader.Lock.Enter(PanGu.Framework.Lock.Mode.Mutex);

                            Segment._ChsName.LoadChsName(_DictionaryDir);
                            _ChsSingleLastTime = GetLastTime(Dict.ChsName.ChsSingleNameFileName);
                            _ChsName1LastTime = GetLastTime(Dict.ChsName.ChsDoubleName1FileName);
                            _ChsName2LastTime = GetLastTime(Dict.ChsName.ChsDoubleName2FileName);
                        }
                        finally
                        {
                            DictionaryLoader.Lock.Leave();
                        }
                    }

                    if (StopWordChanged())
                    {
                        try
                        {
                            DictionaryLoader.Lock.Enter(PanGu.Framework.Lock.Mode.Mutex);

                            Segment._StopWord.LoadStopwordsDict(_DictionaryDir + "Stopword.txt");
                            _StopWordLastTime = GetLastTime("Stopword.txt");
                        }
                        finally
                        {
                            DictionaryLoader.Lock.Leave();
                        }
                    }

                    if (Segment._Synonym.Inited)
                    {
                        if (SynonymChanged())
                        {
                            try
                            {
                                DictionaryLoader.Lock.Enter(PanGu.Framework.Lock.Mode.Mutex);

                                Segment._Synonym.Load(_DictionaryDir);
                                _SynonymLastTime = GetLastTime(Dict.Synonym.SynonymFileName);
                            }
                            finally
                            {
                                DictionaryLoader.Lock.Leave();
                            }
                        }
                    }

                    if (Segment._Wildcard.Inited)
                    {
                        if (WildcardChanged())
                        {
                            try
                            {
                                Segment._Wildcard.Load(_DictionaryDir);
                                _WildcardLastTime = GetLastTime(Dict.Wildcard.WildcardFileName);
                            }
                            finally
                            {
                            }
                        }
                    }

                }
                catch
                {
                }


            }
        }
    }
}
