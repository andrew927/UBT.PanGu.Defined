﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PanGu
{
    /// <summary>
    /// 盘古分词辅助类 add by zhangpeng 2016-10-28
    /// </summary>
    public class PanGuHelper
    {
        /// <summary>
        /// 根据指定词库分词（最基本分词）
        /// </summary>
        /// <param name="strQuestion">用户问题</param>
        /// <param name="configFile">配置文件，物理全路径</param>
        /// <param name="dictName">词库文件，eg：Dict.dct</param>
        /// <param name="isReload">是否重新加载词库，默认不加载，一般在动态加载多个词库时，需要设置此选项</param>
        /// <returns></returns>
        public static string Participle(string strQuestion, string configFile, string dictName, bool isReload = false)
        {
            Segment.Init(configFile, dictName, isReload);
            var options = Setting.PanGuSettings.Config.MatchOptions;
            var parameters = Setting.PanGuSettings.Config.Parameters;

            Segment segment = new Segment();
            //分词
            ICollection<WordInfo> words = segment.DoSegment(strQuestion, options, parameters, dictName);
            return OperateWordInfo(words);
        }

        /// <summary>
        /// 根据指定词库分词(可设置是否包含同义词)
        /// </summary>
        /// <param name="strQuestion">用户问题</param>
        /// <param name="configFile">配置文件，物理全路径</param>
        /// <param name="dictName">词库文件，eg：Dict.dct</param>
        /// <param name="isReload">是否重新加载词库，默认不加载，一般在动态加载多个词库时，需要设置此选项</param>
        /// <param name="isOutPutSync">是否输出同义词</param>
        /// <returns></returns>
        public static string ParticipleSync(string strQuestion, string configFile, string dictName, bool isReload = false, bool isOutPutSync = false)
        {
            Segment.Init(configFile, dictName, isReload);
            var options = Setting.PanGuSettings.Config.MatchOptions;
            var parameters = Setting.PanGuSettings.Config.Parameters;

            options.SynonymOutput = isOutPutSync;

            Segment segment = new Segment();
            //分词
            return segment.DoSegment_SyncRule(strQuestion, options, parameters, dictName);

        }


        /// <summary>
        /// 分词集合处理
        /// </summary>
        /// <param name="words">分词集合</param>
        /// <param name="showPosition">是否显示位置</param>
        /// <returns></returns>
        private static string OperateWordInfo(ICollection<WordInfo> words, bool showPosition = false)
        {
            StringBuilder wordsString = new StringBuilder();
            foreach (WordInfo wordInfo in words)
            {
                if (wordInfo == null)
                {
                    continue;
                }

                if (showPosition)
                {
                    wordsString.AppendFormat("{0}({1},{2})/", wordInfo.Word, wordInfo.Position, wordInfo.Rank);
                }
                else
                {
                    wordsString.AppendFormat("{0}/", wordInfo.Word);
                }
            }
            return wordsString.ToString();
        }

    }
}
