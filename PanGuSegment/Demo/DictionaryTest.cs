﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PanGu;
using PanGu.Dict;

namespace Demo
{
    /// <summary>
    /// 字典测试类
    /// </summary>
    public class DictionaryTest
    {
        public void Test()
        {
            //默认加载，加载配置文件
            PanGu.Segment.Init();
            //获取字典的存放目录，配置文件中
            string dir = PanGu.Setting.PanGuSettings.Config.GetDictionaryPath();
            //创建自定义字典文件
            DictionaryCreate.CreateDictionary(dir, "test_1.dct");

            //插入词库指定的文字
            WordDictionary wordDictionary = new WordDictionary();
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);
            wordDictionary.InsertWord("你好", 0, POS.POS_UNK);

        }
    }
}
